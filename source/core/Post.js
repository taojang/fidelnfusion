enyo.kind({
	name: "FidelnFusion.Core.Post",
	kind: "enyo.Component",
	published: {
		id: "",
		rawid: "",
		created_at: "",
		text: "",
		source: "",
		in_reply_to_status_id: "",
		in_reply_to_user_id: "",
		in_reply_to_screen_name: "",
		repost_status_id: "",
		repost_status: "",
		repost_user_id: "",
		repost_screen_name: "",
		user: {},
		truncated: false
	}
});