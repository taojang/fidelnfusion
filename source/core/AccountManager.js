enyo.kind({
	name: "FidelnFusion.Core.AccountManager",
	kind: "enyo.Component",
	published: {
		accounts: []
	},
	// @public
	/**
	 * load all accounts from db into accounts property
	 */
	loadAllAccounts: function() {
		// initializing the storage
		var store = new Lawnchair(function() {this.name = 'accounts'; this.record = 'account';});
		store.all(enyo.bind(this, funcion(accs){
			this.accounts = accs;
			// fire up an event to notify outside world that all accounts are loaded
		}));
	},
	saveAllAccounts: function() {
	},
	deleteAllAccounts: function() {
		var store = new Lawnchair(function() {this.name = 'accounts'; this.record = 'account';});
		store.nuke(function() {
			// fire up an event to notify all accounts are removed
		});
	},
	loadAccount: function(key) {
	},
	saveAccount: function(key, acc) {
	},
	deleteAccount: function(key) {
	}
});